# Out of Thin Air (OoTA)

[The Problem of Programming Language Concurrency Semantics](https://www.cl.cam.ac.uk/~jp622/the_problem_of_programming_language_concurrency_semantics.pdf)

См. 4. The thin-air problem has no per-candidate-execution solution